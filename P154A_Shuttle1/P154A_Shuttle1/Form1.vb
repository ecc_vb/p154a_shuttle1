﻿' Project:P154A_Shuttle1
' Auther:IE2A No.24, 村田直人
' Date: 2015年06月19日


Public Class Form1

    Dim ResetPoint As Point 'ラベルの初期座標

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.ClientSize = New Size(400, 400 + PlayMenuItem.Height) 'フォームのサイズを設定

        'ラベルのロケーションを設定
        With ShuttleLabel1
            'ラベルの位置を初期化
            .Location = New Point((Panel1.ClientSize.Width - .Size.Width) \ 2, _
                                  (Panel1.ClientSize.Height - .Size.Height) \ 2)
            ResetPoint = ShuttleLabel1.Location 'ラベルの位置(初期化値)を格納
        End With

    End Sub

    'Keyが押された時
    Private Sub Form1_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

        '押されたKeyを検査(シフト無し)
        With ShuttleLabel1
            Select Case e.KeyCode '押されたKeyの方向へ5px移動(シフト無し)

                Case Keys.Left
                    .Location = New Point(.Location.X - 5, .Location.Y) 'ラベルを←に移動    
                Case Keys.Right
                    .Location = New Point(.Location.X + 5, .Location.Y) 'ラベルを→に移動
                Case Keys.Up
                    .Location = New Point(.Location.X, .Location.Y - 5) 'ラベルを↑に移動
                Case Keys.Down
                    .Location = New Point(.Location.X, .Location.Y + 5) 'ラベルを↓に移動
                Case Keys.Escape
                    If .Location.Equals(ResetPoint) = False Then
                        PlayResetMenuItem.PerformClick() 'リセットメニューのクリック処理呼び出し
                    End If
                Case Keys.None
                    Exit Sub 'プロシージャを終了
            End Select
        End With

        '押されたKeyを検査(シフトあり)
        With ShuttleLabel1
            Select Case e.KeyData '押されたKeyの方向へ5px移動(シフトあり)

                Case (Keys.Shift Or Keys.Left)
                    .Location = New Point(.Location.X - 5, .Location.Y) 'ラベルを←に移動    
                Case (Keys.Shift Or Keys.Right)
                    .Location = New Point(.Location.X + 5, .Location.Y) 'ラベルを→に移動
                Case (Keys.Shift Or Keys.Up)
                    .Location = New Point(.Location.X, .Location.Y - 5) 'ラベルを↑に移動
                Case (Keys.Shift Or Keys.Down)
                    .Location = New Point(.Location.X, .Location.Y + 5) 'ラベルを↓に移動
                Case Keys.Escape
                    If .Location.Equals(ResetPoint) = False Then
                        PlayResetMenuItem.PerformClick() 'リセットメニュー
                    End If
                Case Keys.None
                    Exit Sub 'プロシージャを終了
            End Select
        End With


    End Sub

    Private Sub ShuttleLabel1_LocationChanged _
        (ByVal sender As Object, ByVal e As EventArgs) _
        Handles Me.Shown, ShuttleLabel1.LocationChanged

        If ShuttleLabel1.Location.Equals(ResetPoint) Then
            PlayResetMenuItem.Enabled = False '無効化
        Else
            PlayResetMenuItem.Enabled = True '有効化
        End If

    End Sub

    Private Sub PlayResetMenuItem_Click(sender As Object, e As EventArgs) Handles PlayResetMenuItem.Click

        If MessageBox.Show("シャトルを画面の中央に移動します。", Me.Text, _
                           MessageBoxButtons.OKCancel, MessageBoxIcon.Information) _
                            = Windows.Forms.DialogResult.OK Then

            ShuttleLabel1.Location = ResetPoint 'ラベルの位置を初期化
        Else
            Exit Sub 'プロシージャを終了

        End If



    End Sub

    Private Sub PlayExitMenuItem_Click(sender As Object, e As EventArgs) Handles PlayExitMenuItem.Click
        Me.Close() 'フォームを閉じる
    End Sub

  
End Class
